//
//  RecordingsViewController.swift
//  For The Record
//
//  Created by Alex de Waal on 2019/10/27.
//  Copyright © 2019 symptom. All rights reserved.
//

import UIKit
import AVFoundation

protocol RecordingsViewControllerDelegate: class {
    func didStartPlayback()
    func didFinishPlayback()
}

class RecordingsViewController: UIViewController {
    
    //MARK:- Properties
    private var identifier = "cell"
    private var recordings: [Recording] = []
    private var filteredRecordings: [Recording] = []
    
    weak var recordingsDelegate: RecordingsViewControllerDelegate?
    weak var delegate: RecordingsViewControllerDelegate?
    
    let searchController = UISearchController(searchResultsController: nil)
    
    var isSearchBarEmpty: Bool {
      return searchController.searchBar.text?.isEmpty ?? true
    }
    
    var isFiltering: Bool {
      return searchController.isActive && !isSearchBarEmpty
    }
    
    //MARK:- Outlets
    @IBOutlet var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        // MARK: searchController Stuff
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search Recordings"
        navigationItem.searchController = searchController
        definesPresentationContext = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.loadRecordings()
    }
    
    func loadRecordings() {
        self.recordings.removeAll()
        
        let filemanager = FileManager.default
        let documentsDirectory = filemanager.urls(for: .documentDirectory, in: .userDomainMask)[0]
        
        do {
            let paths = try filemanager.contentsOfDirectory(at: documentsDirectory, includingPropertiesForKeys: nil, options: [])
            for path in paths {
                if let spokenWords = UserDefaults.standard.string(forKey: path.lastPathComponent) {
                    let recording = Recording(name: path.lastPathComponent, path: path, spokenText: spokenWords)
                    self.recordings.append(recording)
                }
            }
            self.tableView.reloadData()
        } catch {
            print(error)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toRecording" {
            if let viewController = segue.destination as? ViewRecordingViewController {
                let recording = sender as? Recording
                viewController.recording = recording
                viewController.navigationItem.title = recording?.name
            }
        }
    }

}

extension RecordingsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let recording: Recording
        
        if isFiltering {
            recording = filteredRecordings[indexPath.row]
        } else {
            recording = recordings[indexPath.row]
        }
        
        performSegue(withIdentifier: "toRecording", sender: recording)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let result = self.recordings.count
        
        if result > 0 {
            if isFiltering {
                self.tableView.isHidden = false
                return filteredRecordings.count
            }
        }
        else {
            self.tableView.isHidden = true
        }
        
        return result
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath)
        let recording: Recording
        
        if isFiltering {
            recording = filteredRecordings[indexPath.row]
        } else {
            recording = recordings[indexPath.row]
        }
        
        cell.textLabel?.text = recording.name
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let filemanager = FileManager.default
            let recording = self.recordings[indexPath.row]
            do {
                try filemanager.removeItem(at: recording.path)
                self.recordings.remove(at: indexPath.row)
                self.tableView.reloadData()
            }catch(let err){
                print("Error while deleteing \(err)")
            }
        }
    }
    
    func filterContentForSearchText(_ searchText: String, text: String? = nil) {
        filteredRecordings = recordings.filter({ (recording: Recording) -> Bool in
            return recording.spokenText.lowercased().contains(searchText.lowercased())
        })
      
      tableView.reloadData()
    }
}

extension RecordingsViewController: UISearchResultsUpdating {
  func updateSearchResults(for searchController: UISearchController) {
    let searchBar = searchController.searchBar
    filterContentForSearchText(searchBar.text!)
  }
}
