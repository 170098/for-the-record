//
//  ViewRecordingViewController.swift
//  For The Record
//
//  Created by Alex de Waal on 2019/10/28.
//  Copyright © 2019 symptom. All rights reserved.
//

import UIKit
import AVFoundation

protocol ViewRecordingsViewControllerDelegate: class {
    func didStartPlayback()
    func didFinishPlayback()
}

class ViewRecordingViewController: UIViewController {
    
    var audioTimer: Timer?
    var recording: Recording?
    var audioPlayer: AVAudioPlayer?
    let timeFormatter = NumberFormatter()
    var recordingDelegate: ViewRecordingsViewControllerDelegate?

    @IBOutlet weak var playPauseButton: UIButton!
    @IBOutlet weak var spokenWordsTextView: UITextView!
    @IBOutlet weak var progressBar: UIProgressView!
    @IBOutlet weak var timeLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        if self.isPlaying() {
            setupProgressBar(for: .Stop)
            self.stopPlay()
        }
        
        timeFormatter.minimumIntegerDigits = 2
        timeFormatter.minimumFractionDigits = 0
        timeFormatter.roundingMode = .down
        
        formatSpokenWords()
        setupProgressBar(for: .Stop)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if self.isPlaying() {
            setupProgressBar(for: .Stop)
            self.stopPlay()
        }
        super.viewWillDisappear(animated)
    }
    
    func formatSpokenWords() {
        guard let spokenWords = recording?.spokenText else {
            return
        }
        
        let firstFormat = spokenWords.replacingOccurrences(of: "  ", with: "\n\n")
        let finalFormat = firstFormat.replacingOccurrences(of: "%HESITATION", with: ",")
        
        spokenWordsTextView.text = finalFormat
    }
    
    @IBAction func onPlayPauseTap(_ sender: UIButton) {
        guard let path = recording?.path else {
            return
        }
        makeTimer()
        setupProgressBar(for: .Play)
        self.play(url: path)
    }
    
    @IBAction func onRenameTap(_ sender: UIButton) {
        promptForRecordingName {
            self.navigationController?.popViewController(animated: true)
        }
    }
}

extension ViewRecordingViewController {
    func makeTimer() {
      if audioTimer != nil {
        audioTimer!.invalidate()
      }
        
      audioTimer = Timer.scheduledTimer(timeInterval: 1,
                                        target: self,
                                        selector: #selector(onTimer(timer:)),
                                        userInfo: nil,
                                        repeats: true)
    }
    
    @objc func onTimer(timer: Timer) {
      // Check the audioPlayer, it's optinal remember. Get the current time and duration
      guard let currentTime = audioPlayer?.currentTime, let duration = audioPlayer?.duration else {
        return
      }
      
      let mins = currentTime / 60
      let secs = currentTime.truncatingRemainder(dividingBy: 60)
      let percentCompleted = currentTime / duration
      
      guard let minsStr = timeFormatter.string(from: NSNumber(value: mins)), let secsStr = timeFormatter.string(from: NSNumber(value: secs)) else {
        return
      }
      
      timeLabel.text = "\(minsStr):\(secsStr)"
      progressBar.progress = Float(percentCompleted)
    }
    
    func setupProgressBar(for state: PlayState) {
        switch state {
        case .Play:
            break
        case .Pause:
            break
        case .Stop:
            timeLabel.text = "00:00"
            progressBar.progress = 0
            break
        }
    }
}

extension ViewRecordingViewController {
    private func play(url: URL) {
        
        let image = UIImage(systemName: "pause.fill")
        playPauseButton.setImage(image, for: .normal)
        
        if let delegate = self.recordingDelegate {
            delegate.didStartPlayback()
        }
        
        do {
            let session = AVAudioSession.sharedInstance()
            try session.setCategory(.playback, mode: .spokenAudio)
            try session.setActive(true)
        } catch let error as NSError {
            print(error.localizedDescription)
            return
        }
        
        do {
            let data = try Data(contentsOf: url)
            self.audioPlayer = try AVAudioPlayer(data: data, fileTypeHint: AVFileType.caf.rawValue)
        } catch let error as NSError {
            print(error.localizedDescription)
            return
        }
        
        if let player = self.audioPlayer {
            player.delegate = self
            
            player.volume = AVAudioSession.sharedInstance().outputVolume
            player.prepareToPlay()
            player.play()
        }
        
        if self.isPlaying() {
            self.audioPlayer?.play()
        } else {
            self.audioPlayer?.pause()
        }
    }
    
    func stopPlay() {
        
        let image = UIImage(systemName: "play.fill")
        playPauseButton.setImage(image, for: .normal)
        
        if let delegate = self.recordingDelegate {
            delegate.didFinishPlayback()
        }
        
        if let player = self.audioPlayer {
            player.pause()
        }
        
        self.audioPlayer = nil
        
        do {
            try AVAudioSession.sharedInstance().setActive(false)
        } catch  let error as NSError {
            print(error.localizedDescription)
            return
        }
    }
    
    private func isPlaying() -> Bool {
        if let player = self.audioPlayer {
            return player.isPlaying
        }
        return false
    }
}

extension ViewRecordingViewController: AVAudioPlayerDelegate {
    func audioPlayerDecodeErrorDidOccur(_ player: AVAudioPlayer, error: Error?) {
        if let error = error {
            print(error.localizedDescription)
        }
        self.stopPlay()
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        self.stopPlay()
    }
}

extension ViewRecordingViewController {
    func promptForRecordingName(completion: (() -> Void)? = nil) {
        let alertView = UIAlertController(title: "Rename Recording", message: "Enter a name for the recording", preferredStyle: .alert)
        alertView.addTextField()
        alertView.textFields![0].text = self.recording?.name
        alertView.textFields![0].clearButtonMode = .always
        
        let submitAction = UIAlertAction(title: "Save", style: .default) { [unowned alertView] _ in
            do {
                if let recordingName = self.recording?.name {
                    if let newName = alertView.textFields![0].text {
                        UserDefaults.standard.set(self.recording?.spokenText, forKey: newName)
                        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
                        let documentDirectory = URL(fileURLWithPath: path)
                        let originPath = documentDirectory.appendingPathComponent(recordingName)
                        let destinationPath = documentDirectory.appendingPathComponent(newName)
                        try FileManager.default.moveItem(at: originPath, to: destinationPath)
                    }
                }
            } catch {
                print(error)
            }
            completion?()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)

        alertView.addAction(submitAction)
        alertView.addAction(cancelAction)
        present(alertView, animated: true)
    }
}
