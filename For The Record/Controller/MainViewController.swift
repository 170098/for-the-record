//
//  MainViewController.swift
//  For The Record
//
//  Created by Alex de Waal on 2019/10/27.
//  Copyright © 2019 symptom. All rights reserved.
//

import UIKit
import Pulsator
import Accelerate
import AVFoundation
import SpeechToText
import SPPermission

protocol MainViewControllerDelegate: class {
    func didStartRecording()
    func didFinishRecording()
}

class MainViewController: UIViewController {
    
    let settings = [
        AVFormatIDKey: kAudioFormatLinearPCM,
        AVLinearPCMBitDepthKey: 16,
        AVLinearPCMIsFloatKey: true,
        AVSampleRateKey: Float64(44100),
        AVNumberOfChannelsKey: 1
        ] as [String : Any]
    
    let audioEngine = AVAudioEngine()
    let pulsator = Pulsator()
    var player: AVAudioPlayer!
    
    private var renderTs: Double = 0
    private var recordingTs: Double = 0
    private var silenceTs: Double = 0
    private var audioFile: AVAudioFile?
    private var fileName = ""
    
    var speechToText: SpeechToText!
    var accumulator = SpeechRecognitionResultsAccumulator()
    
    @IBOutlet weak var waveformView: AudioVisualizerView!
    @IBOutlet weak var recordButton: RecordButton!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var pulseView: UIView!
    @IBOutlet weak var viewRecordingsButton: UIBarButtonItem!
    
    weak var delegate: MainViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupRecordingButton()
        setupTimeLabel()
        setupAudioView()
        
        if AVAudioSession.sharedInstance().recordPermission != .granted {
            SPPermission.Dialog.request(
                with: [.microphone],
                on: self,
                dataSource: self
            )
        }
        
        // IBM Authentication
        let authenticator = WatsonIAMAuthenticator(apiKey: Credentials.SpeechToTextApiKey)
        speechToText = SpeechToText (authenticator: authenticator)
        speechToText.serviceURL = "https://gateway-lon.watsonplatform.net/speech-to-text/api"
    }
    
    // MARK: - Start Button Pulse Animation
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupPulseView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let notificationName = AVAudioSession.interruptionNotification
        NotificationCenter.default.addObserver(self, selector: #selector(handleRecording(_:)), name: notificationName, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
}

//MARK:- Update User Interface
extension MainViewController {
    private func updateUI(_ recorderState: RecorderState) {
        switch recorderState {
        case .recording:
            UIApplication.shared.isIdleTimerDisabled = true
            self.waveformView.isHidden = false
            self.timeLabel.isHidden = false
            break
        case .stopped:
            UIApplication.shared.isIdleTimerDisabled = false
            self.waveformView.isHidden = true
            self.timeLabel.isHidden = true
            break
        case .denied:
            UIApplication.shared.isIdleTimerDisabled = false
            self.recordButton.isHidden = true
            self.waveformView.isHidden = true
            self.timeLabel.isHidden = true
            break
        }
    }
}

// MARK: - Recording Functions
extension MainViewController {
    private func startRecording() {
        if let d = self.delegate {
            d.didStartRecording()
        }
        
        self.recordingTs = NSDate().timeIntervalSince1970
        self.silenceTs = 0
        
        do {
            let session = AVAudioSession.sharedInstance()
            try session.setCategory(.playAndRecord, mode: .default)
            try session.setActive(true)
        } catch let error as NSError {
            print(error.localizedDescription)
            return
        }
        
        let inputNode = self.audioEngine.inputNode
        guard let format = self.format() else {
            return
        }
        
        inputNode.installTap(onBus: 0, bufferSize: 1024, format: format) { (buffer, time) in
            let level: Float = -50
            let length: UInt32 = 1024
            buffer.frameLength = length
            let channels = UnsafeBufferPointer(start: buffer.floatChannelData, count: Int(buffer.format.channelCount))
            var value: Float = 0
            vDSP_meamgv(channels[0], 1, &value, vDSP_Length(length))
            var average: Float = ((value == 0) ? -100 : 20.0 * log10f(value))
            if average > 0 {
                average = 0
            } else if average < -100 {
                average = -100
            }
            let silent = average < level
            let ts = NSDate().timeIntervalSince1970
            if ts - self.renderTs > 0.1 {
                let floats = UnsafeBufferPointer(start: channels[0], count: Int(buffer.frameLength))
                let frame = floats.map({ (f) -> Int in
                    return Int(f * Float(Int16.max))
                })
                DispatchQueue.main.async {
                    let seconds = (ts - self.recordingTs)
                    self.timeLabel.text = seconds.toTimeString
                    self.renderTs = ts
                    let len = self.waveformView.waveforms.count
                    for i in 0 ..< len {
                        let idx = ((frame.count - 1) * i) / len
                        let f: Float = sqrt(1.5 * abs(Float(frame[idx])) / Float(Int16.max))
                        self.waveformView.waveforms[i] = min(49, Int(f * 50))
                    }
                    self.waveformView.active = !silent
                    self.waveformView.setNeedsDisplay()
                }
            }
            
            let write = true
            
            if write {
                if self.audioFile == nil {
                    self.audioFile = self.createAudioRecordFile()
                }
                if let f = self.audioFile {
                    do {
                        try f.write(from: buffer)
                    } catch let error as NSError {
                        print(error.localizedDescription)
                    }
                }
            }
        }
        
        do {
            self.audioEngine.prepare()
            try self.audioEngine.start()
        } catch let error as NSError {
            print(error.localizedDescription)
            return
        }
        
        self.updateUI(.recording)
        
    }
    
    private func stopRecording() {
        UserDefaults.standard.set(self.accumulator.bestTranscript, forKey: fileName)
        self.speechToText.stopRecognizeMicrophone()
        
        if let d = self.delegate {
            d.didFinishRecording()
        }
        
        self.audioFile = nil
        self.audioEngine.inputNode.removeTap(onBus: 0)
        self.audioEngine.stop()
        
        do {
            try AVAudioSession.sharedInstance().setActive(false)
        } catch  let error as NSError {
            print(error.localizedDescription)
            return
        }
        
        self.updateUI(.stopped)
        
    }
    
    private func checkPermissionAndRecord() {
        let permission = AVAudioSession.sharedInstance().recordPermission
        switch permission {
        case .undetermined:
            AVAudioSession.sharedInstance().requestRecordPermission({ (result) in
                DispatchQueue.main.async {
                    if result {
                        self.startRecording()
                    }
                    else {
                        self.updateUI(.denied)
                    }
                }
            })
            break
        case .granted:
            self.startRecording()
            break
        case .denied:
            self.updateUI(.denied)
            break
        @unknown default:
            fatalError()
        }
    }
    
    private func isRecording() -> Bool {
        if self.audioEngine.isRunning {
            return true
        }
        return false
    }
    
    private func format() -> AVAudioFormat? {
        let format = AVAudioFormat(settings: self.settings)
        return format
    }
    
    // MARK:- Paths and files
    private func createAudioRecordPath() -> URL? {
        let format = DateFormatter()
        format.dateFormat="yyyy-MM-dd-HH-mm"
        let currentFileName = "recording-\(format.string(from: Date()))"
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        fileName = currentFileName
        let url = documentsDirectory.appendingPathComponent(currentFileName)
        return url
    }
    
    private func createAudioRecordFile() -> AVAudioFile? {
        guard let path = self.createAudioRecordPath() else {
            return nil
        }
        do {
            let file = try AVAudioFile(forWriting: path, settings: self.settings, commonFormat: .pcmFormatFloat32, interleaved: true)
            return file
        } catch let error as NSError {
            print(error.localizedDescription)
            return nil
        }
    }
    
    // MARK: - Handle interruption
    @objc func handleInterruption(notification: Notification) {
        guard let userInfo = notification.userInfo else { return }
        guard let key = userInfo[AVAudioSessionInterruptionTypeKey] as? NSNumber
            else { return }
        if key.intValue == 1 {
            DispatchQueue.main.async {
                if self.isRecording() {
                    self.stopRecording()
                }
            }
        }
    }
}

// MARK: - Machine Learning
extension MainViewController {
    @objc func handleRecording(_ sender: RecordButton) {
        if recordButton.isRecording {
            pulsator.stop()
            viewRecordingsButton.isEnabled = false
            var settings = RecognitionSettings(contentType: "audio/ogg;codecs=opus")
            settings.interimResults = true
            
            var callback = RecognizeCallback()
            callback.onError = { error in
                print(error)
            }
            
            callback.onResults = { results in
                self.accumulator.add(results: results)
            }

            self.speechToText.recognizeMicrophone(settings: settings, callback: callback)
            
            waveformView.isHidden = false
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.timeLabel.alpha = 1
                self.waveformView.alpha = 1
                self.view.layoutIfNeeded()
            }, completion: nil)
            self.checkPermissionAndRecord()
        } else {
            pulsator.start()
            viewRecordingsButton.isEnabled = true
            waveformView.isHidden = true
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.timeLabel.alpha = 0
                self.waveformView.alpha = 0
                self.view.layoutIfNeeded()
            }, completion: nil)
            self.stopRecording()
        }
    }
}

// MARK: - Setup UI
extension MainViewController {
    fileprivate func setupRecordingButton() {
        recordButton.isRecording = false
        recordButton.addTarget(self, action: #selector(handleRecording(_:)), for: .touchUpInside)
    }
    
    fileprivate func setupTimeLabel() {
        timeLabel.translatesAutoresizingMaskIntoConstraints = false
        timeLabel.text = "00:00"
        timeLabel.textColor = .gray
        timeLabel.alpha = 0
    }
    
    fileprivate func setupAudioView() {
        waveformView.alpha = 0
        waveformView.isHidden = true
    }
    
    fileprivate func setupPulseView() {
        pulseView.layer.addSublayer(pulsator)
        pulsator.position = CGPoint(x: (pulseView.bounds.height / 2) - 3, y: (pulseView.bounds.width / 2) + 2)
        pulsator.backgroundColor = UIColor.red.cgColor
        pulsator.repeatCount = 50
        pulsator.radius = 100.0
        pulsator.numPulse = 2
        pulsator.start()
    }
}

// MARK: - Permission Dialog
extension MainViewController: SPPermissionDialogDataSource {
    var showCloseButton: Bool {
        return true
    }
    
    var dialogTitle: String { return "Need Access Please" }
    var dialogSubtitle: String { return "Permissions Request" }
    var dialogComment: String { return "" }
    var allowTitle: String { return "Allow" }
    var allowedTitle: String { return "Allowed" }
    var bottomComment: String { return "None of the information will be sent to a server. Everything is saved on the device." }
    
    func name(for permission: SPPermissionType) -> String? { return nil }
    func description(for permission: SPPermissionType) -> String? { return nil }
    func deniedTitle(for permission: SPPermissionType) -> String? { return nil }
    func deniedSubtitle(for permission: SPPermissionType) -> String? { return nil }

    var cancelTitle: String { return "Cancel" }
    var settingsTitle: String { return "Settings" }
}
