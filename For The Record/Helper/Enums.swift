//
//  Enums.swift
//  For The Record
//
//  Created by Alex de Waal on 2019/10/26.
//  Copyright © 2019 symptom. All rights reserved.
//

import Foundation

enum RecorderState {
    case recording
    case stopped
    case denied
}

enum PlayState {
    case Play
    case Pause
    case Stop
}
