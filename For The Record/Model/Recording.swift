//
//  Recording.swift
//  For The Record
//
//  Created by Alex de Waal on 2019/10/26.
//  Copyright © 2019 symptom. All rights reserved.
//

import Foundation

class Recording {
    var name: String
    var path: URL
    var spokenText: String
    
    init(name: String, path: URL, spokenText: String) {
        self.name = name
        self.path = path
        self.spokenText = spokenText
    }
}
