//
//  UIView+Designable.swift
//  For The Record
//
//  Created by Alex de Waal on 2019/10/26.
//  Copyright © 2019 symptom. All rights reserved.
//

import UIKit

@IBDesignable
class DesignableView: UIView {
    
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        } set {
            layer.cornerRadius = newValue
        }
    }
    
}
