# For The Record

This project makes use of Cocoapods for the IBM Watson Swift SDK and also for UI Libraries. To get started using the project, open terminal and navigate to the project's root directory and type the following: 

    pod install

Once that is complete you'll notice a new **.xcworkspace** file in the project folder, make sure to open that one.

That's it